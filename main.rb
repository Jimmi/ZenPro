require 'nokogiri'
require 'open-uri'
require 'pp'
require 'mechanize'
require 'active_record_migrations'
require 'active_record'
# Coding: UTF-8

INSERT = false
config = YAML::load(File.open('config/papers.yml'))

project_root = File.dirname(File.absolute_path(__FILE__))
Dir.glob(project_root + "/app/models/*.rb").each{|f| require f}

## initialice the database
connection_details = YAML::load(File.open('db/config.yml'))
development_details = connection_details["development"]
ActiveRecord::Base.establish_connection(development_details)

# begin
  Paper.where(:url != "").find_each do |paper|
    pp paper
    main_page = Nokogiri::HTML(open(paper.url))

    number_of_articles = 1
    main_page.xpath(config[paper.yml_selector]['header']).each do |header|
      case paper.yml_selector
      when "spiegel"
        article_handler = ArticleHandler.do_spiegel(header, config, paper)
      when "welt"
        article_handler = ArticleHandler.do_welt(header, config, paper)
      end
      if !article_handler.nil?
        pp ">>> INSERT <<<"
        pp "HEADER = "  +article_handler["header"]
        pp "URL = " + article_handler["url"]
        pp "TIME = " + Time.now
        pp false
        pp "TEXT = " + article_handler["text"].length
        pp "CATEGORY = " + article_handler["category"]
        pp "IS COMMENT ACTIV = " + article_handler["is_comment_activ"]
        pp "NUMBER OF COMMENTS = " + article_handler["number_of_comments"]
        pp "NUMBER OF ARTICLES = " + number_of_articles

        if INSERT
          article = Article.new(
            header: article_handler["header"],
            url: article_handler["url"],
            created_at: Time.now,
            is_fiddle: false,
            text: article_handler["text"],
            category: article_handler["category"],
            is_comment_activ: article_handler["is_comment_activ"],
            number_of_comments: article_handler["number_of_comments"]
          )
          article.save!
          pp ">>> INSERT END <<<"

          paper = Paper.find_by(url: paper.url)
          paper.last_inspection = Time.now
          paper.number_of_articles = paper.number_of_articles.to_i + number_of_articles.to_i
          paper.save!
        end
        number_of_articles += 1
      end
    end
  end
# rescue
#   puts ":(" * 100
# end
