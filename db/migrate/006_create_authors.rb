require 'active_record'

class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :name, :null => false
      t.integer :number_of_articles
      t.timestamps
      t.text :notice
    end
  end
end
