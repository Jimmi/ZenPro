require 'active_record'

class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :header, :null => false
      t.text :text, :null => false
      t.timestamps
      t.timestamp :last_inspection
      t.integer :author_id
      t.integer :number_of_comments
      t.boolean :is_fiddle, :null => false
      t.text :url, :null => false
      t.string :category
      t.boolean :is_comment_activ
    end
  end
end
