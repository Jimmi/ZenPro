require 'active_record'

class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :title, :null => false
      t.integer :number
      t.text :text, :null => false
      t.timestamps
      t.timestamp :last_inspection
      t.boolean :is_fiddle, :null => false
      t.integer :user_id, :null => false
    end
  end
end
