require 'active_record'

class CreatePapers < ActiveRecord::Migration
  def change
    create_table :papers do |t|
      t.string :name, :null => false
      t.text :url, :null => false
      t.timestamps
      t.timestamp :last_inspection
      t.integer :number_of_articles
      t.string :yml_selector
    end
  end
end
