require 'active_record'

class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, :null => false
      t.integer :number_of_comments
      t.timestamps
    end
  end
end
