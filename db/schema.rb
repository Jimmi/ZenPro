# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 6) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: true do |t|
    t.string   "header",             null: false
    t.text     "text",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_inspection"
    t.integer  "author_id"
    t.integer  "number_of_comments"
    t.boolean  "is_fiddle",          null: false
    t.text     "url",                null: false
    t.string   "category"
    t.boolean  "is_comment_activ"
  end

  create_table "authors", force: true do |t|
    t.string   "name",               null: false
    t.integer  "number_of_articles"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "notice"
  end

  create_table "comments", force: true do |t|
    t.string   "title",           null: false
    t.integer  "number"
    t.text     "text",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_inspection"
    t.boolean  "is_fiddle",       null: false
    t.integer  "user_id",         null: false
  end

  create_table "comments_users", id: false, force: true do |t|
    t.integer "user_id",    null: false
    t.integer "comment_id", null: false
  end

  create_table "papers", force: true do |t|
    t.string   "name",               null: false
    t.text     "url",                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_inspection"
    t.integer  "number_of_articles"
    t.string   "yml_selector"
  end

  create_table "users", force: true do |t|
    t.string   "name",               null: false
    t.integer  "number_of_comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
