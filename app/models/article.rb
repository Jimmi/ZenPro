class Article < ActiveRecord::Base
  belongs_to :paper
  has_many :comments
  belongs_to :author
end
