class ArticleHandler
  def self.do_welt(header, config, paper)
    artcile_hash = Hash.new

    url = header.children[0].attributes.values[0].value rescue nil
    found_header = Article.find_by header: header.text.delete!("\n")

    if found_header.nil? && !url.nil?

      article_text = ""
      article_page = Nokogiri::HTML(open(url))

      number_of_comments = 0
      is_comment_active = false
      if article_page.css(".dsqCommentCount").text.match(/\d+/).nil?
        number_of_comments = 0
        is_comment_active = false
      else
        number_of_comments = article_page.css(".dsqCommentCount").text.match(/\d+/).to_s.to_i
        is_comment_active = true
      end

      article_page.xpath(config[paper.yml_selector]['article']).each do |l|
        article_text << l.text
      end

      category = article_page.xpath(config[paper.yml_selector]['category']).text
      article_text = article_text.delete!("\n")


      if !category.nil? && !article_text.nil?
        artcile_hash = Hash[
          "header", header.text.delete!("\n"),
          "url", url,
          "text", article_text,
          "category", category,
          "number_of_comments", number_of_comments,
          "is_comment_active", is_comment_active
          ]

      end
    end
    if artcile_hash.count == 0
      nil
    else
      artcile_hash
    end
  end

  def self.do_spiegel(header, config, paper)
    artcile_hash = Hash.new
    category = header.children[1].children.children[0].to_s.encode("UTF-8")
    url = "http://spiegel.de" + header.children[1].attributes['href'].value
    header = header.children[1].children.children[1].to_s.encode("UTF-8")

    found_header = Article.find_by header: header

    if found_header.nil? && !url.nil?

      article_page = Nokogiri::HTML(open(url))
      number_of_comments = 0
      is_comment_active = false
      if article_page.css("#spArticleFunctionForum>a").text.match(/\d+/).nil?
        number_of_comments = 0
        is_comment_active = false
      else
        number_of_comments = article_page.css("#spArticleFunctionForum>a").text.match(/\d+/).to_s.to_i
        is_comment_active = true
      end

      article_text = ""
      article_page.xpath(config[paper.yml_selector]['article']).each do |l|
        article_text << l.text
      end
      article_text = article_text.gsub(/\n/, '').gsub(/\t/, '').gsub(/<!--.*?-->/, '')

      if !category.nil? && !article_text.nil?
        artcile_hash = Hash[
          "header", header,
          "url", url,
          "text", article_text,
          "category", category,
          "number_of_comments", number_of_comments,
          "is_comment_active", is_comment_active
          ]

      end
    end
    if artcile_hash.count == 0
      nil
    else
      artcile_hash
    end
  end
end
